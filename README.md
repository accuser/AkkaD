**************记录在码云上增加README.md的方法**************  
  
操作系统是Mac Pro  

cd /Users/yanxin/IdeaProjects/AkkD  

touch README.md //创建帮助文件  

git add README.md //添加文件    

可省略步骤:(git add .  //添加工程目录下所有文件)
  
git commit -m "注释内容” // 添加注释    
  
git push -u origin master //推送要上传的文件  
  
********************************************************      

**************这个工程的功能如下**************  
使用Scala编写RPC框架

分为Master和Worker两部分:分别启动两个端

Worker会将注册信息发给Master,Master收到信息并封装保存到内存中(集合中),注册完成后,Master再向Worker返回信息,告知保存成功.

Master发送完返回信息后,Worker再向Master发送心跳,并且定期发送心跳到Master中,目的是为了告知Master,报活,并更新Master上的心跳时间.

此时在Master中有一个定时器,定时检查Worker的超时时间.封装的数据形式是一个集合,并且通过filter的过滤条件将超时的work从集合中去除.

过滤条件为:当前时间-上次心跳的时间 大于 定义的固定检查时间范围,则视为超时worker,则去除.

之所以这样设计,是因为分布式计算,在集群中使用.具体的执行工作是在Worker进程中进行(确切的说是在Worker上的子进程执行);

这样Master就知道了哪些Worker是活着的,并且知道Worker上的资源信息.所以,Master可以通过分配工作对Worker进行调度.

这些大概就是Spark底层的简单通信,虽然在spark2.0中使用了netty,来替换了akka,但是原理上是类似的.


