package AkkaD

/**
  * 改造第十二步:
  * 定义一个封装Worker的信息类
  */
class WorkerInfo(val id:String,val memory:Int,val cores:Int) {

  //记录上一次心跳
  var lastHeartbeatTime:Long = _
}
