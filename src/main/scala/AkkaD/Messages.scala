package AkkaD

/**
  * 对应一个消息对象
  */
trait Messages extends Serializable


//Master -> Master 内部消息(不用序列化,因为这是内部消息)
case object CheckTimeOutWorker

//Worker -> Master 注册时传递的消息(包括内容:id,cores,cpu)
//需要实现Serializable,因为要通过网络实现序列化
case class RegisterWorker(workerId:String,memory:Int,cores:Int) extends Messages

//接收已经在Master注册过的Worker定时发送来的心跳 Worker -> Master
case class HeartBeat(workerId:String) extends Messages

//Master -> Worker Master反馈给Worker已经注册的Worker有哪些
case class RegisteredWorker(masterUrl:String) extends Messages

//更新:Worker -> self
case object SendHeartBeat